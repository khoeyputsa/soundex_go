FROM golang:1.13.5-alpine as builder
WORKDIR /go/src/
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app .

FROM anapsix/alpine-java
WORKDIR /go/src/app
COPY --from=0 /go/src  .
CMD ["./app"]