package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os/exec"
)

type ResponseSoundex struct {
	Keyword string `json:"keyword"`
	Score   string `json:"score"`
	Time    string `json:"time"`
}

type Output struct {
	Status  string            `json:"status"`
	Message string            `json:"message"`
	Data    []ResponseSoundex `json:"data"`
}

func main() {
	http.HandleFunc("/", soundex)
	http.ListenAndServe(":8080", nil)
}

func soundex(w http.ResponseWriter, r *http.Request) {
	out, err := exec.Command("java", "-jar", "SoundexAPI-RIV1_old.jar", "-action", "search", "-keyword", "อรรถพล", "-candidate", "3").Output()
	// fmt.Println("Starting command")
	if err != nil {
		fmt.Println(err)
	}
	var res []ResponseSoundex

	s := string(out)
	json.Unmarshal([]byte(s), &res)

	response := Output{
		Status:  "OK",
		Message: "Search successfully!",
		Data:    res,
	}
	json.NewEncoder(w).Encode(response)
}
